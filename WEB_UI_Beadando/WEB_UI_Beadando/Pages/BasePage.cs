﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace WEB_UI_Beadando.Pages
{
    class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
