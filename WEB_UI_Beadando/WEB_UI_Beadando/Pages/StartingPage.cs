﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using WEB_UI_Beadando.Widgets;

namespace WEB_UI_Beadando.Pages
{
    class StartingPage : BasePage
    {
        public StartingPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static StartingPage Navigate(IWebDriver webDriver, string link)
        {
            webDriver.Url = link;
            return new StartingPage(webDriver);
        }

        public ClickWidget GetClickWidget()
        {
            return new ClickWidget(Driver);
        }

        public MoodleResultWidget GetMoodleResultWidget()
        {
            return new MoodleResultWidget(Driver);
        }

        public RedditResultWidget GetRedditResultWidget()
        {
            return new RedditResultWidget(Driver);
        }

        public StackOverflowResultWidget GetNeptunResultWidget()
        {
            return new StackOverflowResultWidget(Driver);
        }
    }
}
