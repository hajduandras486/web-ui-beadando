﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WEB_UI_Beadando.Pages;

namespace WEB_UI_Beadando.Widgets
{
    class StackOverflowResultWidget : BasePage
    {
        public StackOverflowResultWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public bool GetResult(String element)
        {

            return Driver.FindElement(By.Id(element)).Displayed;
        }
    }
}
