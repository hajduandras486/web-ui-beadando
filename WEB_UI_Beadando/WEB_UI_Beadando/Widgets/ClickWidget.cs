﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using WEB_UI_Beadando.Pages;

namespace WEB_UI_Beadando.Widgets
{
    class ClickWidget : BasePage
    {
        public ClickWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public StartingPage ClickOn(String click)
        {
            Driver.FindElement(By.CssSelector(click)).Click();

            return new StartingPage(Driver);
        }

    }
}
