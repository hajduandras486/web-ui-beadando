﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEB_UI_Beadando.Pages;

namespace WEB_UI_Beadando.Widgets
{
    class RedditResultWidget : BasePage
    {

        public RedditResultWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public int GetResult(String element)
        {
            string baseDirectory = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\screenshots\\"));
            string screenshotName = baseDirectory + TestContext.CurrentContext.Test.MethodName + "_" +
                DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + "_error";

            var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
            screenshot.SaveAsFile(screenshotName, ScreenshotImageFormat.Jpeg);

            return Driver.FindElements(By.CssSelector(element)).Count();
        }
    }
}
