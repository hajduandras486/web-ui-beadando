﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEB_UI_Beadando.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace WEB_UI_Beadando.Widgets
{
    class MoodleResultWidget : BasePage
    {
        public MoodleResultWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public bool GetResult(String element)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(3));

            return wait.Until(d => d.FindElement(By.CssSelector(element)).Displayed);
        }

    }
}
