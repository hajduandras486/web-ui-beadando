﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections;
using System.Linq;
using System.Xml.Linq;
using WEB_UI_Beadando.Pages;

namespace WEB_UI_Beadando
{
    class BeadandoPageObjectTest : TestBase
    {

        [Test, TestCase("https://stackoverflow.com/", ".login-link.s-btn.s-btn__filled.py8.js-gps-track", "submit-button")]
        public void TestStackOverflow(String link, String click, String element)
        {

            var result = StartingPage.Navigate(Driver, link)
                .GetClickWidget()
                .ClickOn(click)
                .GetNeptunResultWidget()
                .GetResult(element);

            Assert.True(result);
        }

        [Test, TestCase("https://old.reddit.com/", ".premium-banner", "notexistingelement")]
        public void TestReddit(String link, String click, String element)
        {

            var result = StartingPage.Navigate(Driver, link)
                .GetClickWidget()
                .ClickOn(click)
                .GetRedditResultWidget()
                .GetResult(element);

            Assert.AreEqual(result, 0);
        }

        [Test, TestCaseSource("TestData")]
        public void TestMoodle(String link, String click, String element)
        {

            var result = StartingPage.Navigate(Driver, link)
                .GetClickWidget()
                .ClickOn(click)
                .GetMoodleResultWidget()
                .GetResult(element);

            Assert.True(result);
        }


        static IEnumerable TestData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\data.xml");
            return
                from vars in doc.Descendants("testData")
                let link = vars.Attribute("link").Value
                let click = vars.Attribute("click").Value
                let element = vars.Attribute("element").Value
                select new object[] { link, click, element };
        }
    }

}
